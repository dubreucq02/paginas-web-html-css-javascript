let paginas = document.querySelectorAll(".form__div");
let btnNext = document.querySelector(".btnnext");
let btnPrev = document.querySelector(".btnprev");

let inputs = document.querySelectorAll("input");
let input = document.querySelector("input[name='nombre']");
let pagActual = 0;

function showPage(num) {
    pagActual+=num;
    hidden(pagActual)

    paginas.forEach((page, index) =>
    {
        if(pagActual==index){
            page.classList.add("active");
        }
        else{
            page.classList.remove("active");
        }
    })
}

function hidden(pagina){
    if(pagina==0){
        btnPrev.classList.add("hidden");
    }
    else{
        btnPrev.classList.remove("hidden");
    }

    if((pagina+1)==paginas.length){
        btnNext.classList.add("hidden");
    }
    else{
        btnNext.classList.remove("hidden");
    }
}

btnPrev.addEventListener('click', e =>showPage(-1));
btnNext.addEventListener('click', e =>showPage(1));

inputs[2].focus()
input.addEventListener('keydown', e => {
    console.log(e);
    if(e.key=="d"){
        alert("hola");
    }


});
inputs.forEach((inp, indice)=>{
    inp.addEventListener('keydown', e => {
        if(e.key=="Enter" || e.key=="ArrowDown"){
            e.preventDefault();
            inputs[indice+1].focus();
        }
    });
});