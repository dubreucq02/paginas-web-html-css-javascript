# Import the Tkinter library
from tkinter import *

# Define the main window
root = Tk()

# Define the calculator's main frame
frame = Frame(root)
frame.grid()

# Define the input and output fields
input_field = Entry(frame)
input_field.grid(row=0, column=0, columnspan=3)
output_field = Entry(frame)
output_field.grid(row=1, column=0, columnspan=3)

# Define the function for the buttons
def button_press(number):
    current = input_field.get()
    input_field.delete(0, END)
    input_field.insert(0, str(current) + str(number))

def clear_input():
    input_field.delete(0, END)

def evaluate():
    result = eval(input_field.get())
    output_field.delete(0, END)
    output_field.insert(0, result)

# Define the buttons
button1 = Button(frame, text="1", command=lambda: button_press(1))
button1.grid(row=2, column=0)
button2 = Button(frame, text="2", command=lambda: button_press(2))
button2.grid(row=2, column=1)
button3 = Button(frame, text="3", command=lambda: button_press(3))
button3.grid(row=2, column=2)
button4 = Button(frame, text="4", command=lambda: button_press(4))
button4.grid(row=3, column=0)
button5 = Button(frame, text="5", command=lambda: button_press(5))
button5.grid(row=3, column=1)
button6 = Button(frame, text="6", command=lambda: button_press(6))
button6.grid(row=3, column=2)
button7 = Button(frame, text="7", command=lambda: button_press(7))
button7.grid(row=4, column=0)
button8 = Button(frame, text="8", command=lambda: button_press(8))
button8.grid(row=4, column=1)
button9 = Button(frame, text="9", command=lambda: button_press(9))
button9.grid(row=4, column=2)
button0 = Button(frame, text="0", command=lambda: button_press(0))
button0.grid(row=5, column=0)
button_add = Button(frame, text="+", command=lambda: button_press("+"))
button_add.grid(row=2, column=3)
button_sub = Button(frame, text="-", command=lambda: button_press("-"))
button_sub.grid(row=3, column=3)
button_mul = Button(frame, text="*", command=lambda: button_press("*"))
button_mul.grid(row=4, column=3)
button_div = Button(frame, text="/", command=lambda: button)
