let deportes = document.querySelectorAll('.info__deporte');

for(let i=1; i<=deportes.length; i++){
    let selectorFlechas = '.info__deporte:nth-child('+ i +') .galeria-deporte i';
    let selectorImgs = '.info__deporte:nth-child('+ i +') .galeria__img';
    let flechas = document.querySelectorAll(selectorFlechas);
    let imgsGaleria = document.querySelectorAll(selectorImgs);

    flechas[0].addEventListener('click',()=>{
        let cumplio=false;
        imgsGaleria.forEach((img, indice)=>{
            if(img.classList.contains('galeria__img-active')
            && indice > 0 && !cumplio){
                img.classList.remove('galeria__img-active');
                img.classList.add('galeria__img-right');

                imgsGaleria[indice-1].classList.remove('galeria__img-left');
                imgsGaleria[indice-1].classList.add('galeria__img-active');
                cumplio=true;
                if(indice !== 1){
                    imgsGaleria[indice-2].classList.add('galeria__img-left');
                }
                if(indice !== (imgsGaleria.length-1)){
                    imgsGaleria[indice+1].classList.remove('galeria__img-right');
                }
            }
        })
    })

    flechas[1].addEventListener('click',()=>{
        let cumplio=false;
        imgsGaleria.forEach((img, indice)=>{
            if(img.classList.contains('galeria__img-active')
            && indice < (imgsGaleria.length -1) && !cumplio){
                img.classList.remove('galeria__img-active');
                img.classList.add('galeria__img-left');

                imgsGaleria[indice+1].classList.remove('galeria__img-right');
                imgsGaleria[indice+1].classList.add('galeria__img-active');

                cumplio=true;
                if(indice !== (imgsGaleria.length-2)){
                    imgsGaleria[indice+2].classList.add('galeria__img-right');
                }
                if(indice !==0){
                    imgsGaleria[indice-1].classList.remove('galeria__img-left');
                }
            }
        })
    })
}


