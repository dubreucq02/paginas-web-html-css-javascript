const connection = require('./bd');
console.log(connection);


// Horario de apertura y cierre de la cancha


const horaApertura = 8;
const horaCierre = 23;
const horaCierreDomingo = 13;

// Matriz de horas y minutos
const horas = Array.from({length: (horaCierre - horaApertura) * 2}, (_, i) => {
    const hora = Math.floor(i / 2) + horaApertura;
    const minutos = i % 2 === 0 ? '00' : '30';
    return `${hora}:${minutos}`;
});



//fecha del dia
let fecha = document.querySelector('.fecha');
let date = new Date();
let dias = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado',
'domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
let meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto',
 'septiembre', 'octubre', 'noviembre', 'diciembre'];

fecha.textContent = dias[date.getDay()] + ' ' +date.getDate() + ' de ' + meses[date.getMonth()];






// Obtener la tabla y el cuerpo de la tabla
const tabla = document.getElementById('tabla-canchas');

for(let j=0; j<7; j++){
    const cuerpoTabla = document.createElement('tbody');
    cuerpoTabla.classList.add('horarios-tabla');
    let horasNew = horas;
    if(j==0){
        cuerpoTabla.classList.add('mostrar-tabla');
    }
    if(dias[date.getDay()+j]=='domingo'){
        console.log('domingo');
        horasNew = horas.slice(0,10);
    }

    // Recorrer las horas y agregar filas a la tabla
    horasNew.forEach(hora => {
        const fila = document.createElement('tr');
        const celdaHora = document.createElement('td');
        celdaHora.textContent = hora;
        fila.appendChild(celdaHora);

        for (let i = 0; i < 2; i++) {
            const celdaCancha = document.createElement('td');
            const enlaceDisponible = document.createElement('a');
            enlaceDisponible.href = '#';
            enlaceDisponible.textContent = 'Disponible';
            celdaCancha.appendChild(enlaceDisponible);
            fila.appendChild(celdaCancha);
        }

        cuerpoTabla.appendChild(fila);
        tabla.appendChild(cuerpoTabla);
    });
}


//funcionalidad flechas
let flechas = document.querySelectorAll('.btn-tabla i');
let btnHoy = document.querySelector('.btn-tabla a');
let tablas = document.querySelectorAll('.horarios-tabla');

flechas[0].addEventListener('click', ()=>{
    tablas.forEach((tabla, indice)=>{
        if(tabla.classList.contains('mostrar-tabla') && indice > 0){
            tabla.classList.remove('mostrar-tabla');
            tablas[indice-1].classList.add('mostrar-tabla');
            let nextDays = new Date();
            nextDays.setDate(date.getDate() + indice -1);

            console.log(indice);
            console.log(dias[nextDays.getDay()]);

            fecha.textContent = dias[nextDays.getDay()] + ' ' + nextDays.getDate() + ' de ' + meses[nextDays.getMonth()];
        }
    })
})

flechas[1].addEventListener('click', ()=>{
    let change= false;
    tablas.forEach((tabla, indice)=>{
        if(tabla.classList.contains('mostrar-tabla') && indice < 6 && !change){
            change=true;
            tabla.classList.remove('mostrar-tabla');
            tablas[indice+1].classList.add('mostrar-tabla');
            
            

            let nextDays = new Date();
            nextDays.setDate(date.getDate() + indice + 1);

            console.log(indice);
            console.log(dias[nextDays.getDay()]);

            fecha.textContent = dias[nextDays.getDay()] + ' ' + nextDays.getDate() + ' de ' + meses[nextDays.getMonth()];
        }
    })
})

btnHoy.addEventListener('click', ()=>{
    tablas.forEach((tabla)=>{
        if(tabla.classList.contains('mostrar-tabla')){
            tabla.classList.remove('mostrar-tabla');
        }
    })
    tablas[0].classList.add('mostrar-tabla');
    fecha.textContent = dias[date.getDay()] + ' ' +date.getDate() + ' de ' + meses[date.getMonth()];
})


console.log(connection.query('select * from alquileres'));

connection.end();



