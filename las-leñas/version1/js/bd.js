const mysql = require('../paquetes/node_modules/mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'agustin'
});

connection.connect((error) => {
    if (error) {
      console.error('Error al conectar a la base de datos: ', error);
    } else {
      console.log('Conexión exitosa a la base de datos!');
    }
  });

  connection.query('SELECT * FROM alquileres', (error, results) => {
    if (error) {
      console.error('Error al ejecutar consulta: ', error);
    } else {
      console.log('Resultados de la consulta: ', results);
    }
  });

  connection.end();