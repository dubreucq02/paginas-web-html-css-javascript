let btnUbication = document.querySelector('.ubicacion > div:first-child i');
let ubication = document.querySelector('.ubicacion > div:nth-child(2)');
let btnHorarios = document.querySelector('.horarios > div:first-child i');
let horarios = document.querySelector('.horarios > div:nth-child(2)');

const ocultar = (caja)=>{
    if(caja.classList.contains('oculto')){
        caja.classList.remove('oculto');
    }else{
        caja.classList.add('oculto');
    }
}


btnUbication.addEventListener('click',()=>{
    ocultar(ubication);
});
btnHorarios.addEventListener('click', ()=>{
    ocultar(horarios);
});