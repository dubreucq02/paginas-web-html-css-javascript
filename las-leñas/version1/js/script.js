let nav = document.querySelector('nav');

window.addEventListener('scroll',()=>{
    if(window.scrollY > 10 ||
        window.location.pathname!='/version1/index.html'){
        nav.classList.add('nav-scroll');
    }else{
        nav.classList.remove('nav-scroll');
    }
})

window.addEventListener('load',()=>{
    if(window.location.pathname!='/version1/index.html'){
        nav.classList.add('nav-scroll');
    }else{
        nav.classList.remove('nav-scroll');
    }
})

let bntMenu = document.querySelector('.btn-menu');
let opcionesMenu = document.querySelector('.opciones-menu');
let fondo = document.querySelector('.fondo-oscuro');

bntMenu.addEventListener('click', ()=>{
    console.log('ejecutando')
    if(opcionesMenu.classList.contains('opciones-menu-active')){
        opcionesMenu.classList.remove('opciones-menu-active');
        fondo.classList.remove('aparecer');
        
    }else{
        opcionesMenu.classList.add('opciones-menu-active');
        fondo.classList.add('aparecer');
    }
})

console.log(opcionesMenu, bntMenu);
console.log(window.location.pathname!='/version1/index.html');
console.log(window.scrollY > 10 ||
    window.location.pathname!='/version1/index.html')
